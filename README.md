# pdfutil
PDF转换工具
支持PDF转换为TXT、DOC、HTML  
Java语言开发，运行环境需要安装配置JRE   
inputpdf存放需要转换的PDF文件，转换成功后存放outputpdf  
安装JRE后在软件目录运行：java -jar pdfutil.jar   
[下载](https://gitee.com/ishare20/pdfutil/releases/v1.0)