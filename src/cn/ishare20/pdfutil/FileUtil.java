package cn.ishare20.pdfutil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class FileUtil {
	static ArrayList<String> getDirFileNames(String path){
		
		ArrayList<String> filenames=new ArrayList<String>();
		
		File file=new File(path);
		File[] flist=file.listFiles();
		//System.out.println(file.getAbsolutePath());
		if (flist.length==0) {
			System.out.println("目录不存在文件");
			
		}else {
			
			for (int i = 0; i < flist.length; i++) {
				File fs=flist[i];
				if (fs.isFile()) {
					if (fs.getName().endsWith(".pdf")) {
						filenames.add(fs.getName());
					}
					
					
				}
			}
		}
		return filenames;
		
	}
	
	public static boolean SaveString2Text(String htmlString, String filename){

        File file =new File(filename);
        try {
            FileWriter fileWriter=new FileWriter(file);
            fileWriter.write(htmlString);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (file.exists()){
            return true;
        }
        else {
            return false;
        }
    }
}
