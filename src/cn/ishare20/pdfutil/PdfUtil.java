package cn.ishare20.pdfutil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.tools.PDFText2HTML;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
public class PdfUtil {

	/**
	 * @param args
	 */
		 static String currDir=System.getProperty("user.dir");
		 static String inPut=currDir+File.separator+"inputpdf"+File.separator;
		 static String outPut=currDir+File.separator+"outputpdf"+File.separator;
		 static Config config=ConfigFactory.parseFile(new File("application.conf"));
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner=new Scanner(System.in);
		
		ArrayList<String> fileNames=FileUtil.getDirFileNames(inPut);
		
		
		
		System.out.println(getIntroduce()+"\n");
		
		if (isPage()) {
			System.out.println("当前配置模式\n" +
					"转换类型: "+getOutPutType()+"\n"
		+"模式:指定页数范围转换\n"+"首页:"+getStart()+"\n"+"结束页:"+getEnd());
		}else {
			System.out.println("当前配置模式\n转换类型: "+getOutPutType()+"\n"
					+"模式: 全文转换");
		}
		System.out.println("配置文件: application.conf\n输入quit退出");
		
		
		if (fileNames.size()==0) {
			System.out.println("\n文件夹不存在PDF文件!,请PDF文件放在inputpdf文件夹后重启软件");
			
		}
		
		
		String quit="";
		
		do{
			
		
		
		System.out.println("\n文件夹的所有PDF文件");
		int i;
		for (i = 0; i < fileNames.size(); i++) {
			System.out.println(i+"."+fileNames.get(i));
		}  
		
		System.out.println(i+".所有PDF文件");
		System.out.println("\n请输入所需的PDF编号:");
		
		
		
		String scStr=scanner.next();
		int fileNo=0;
		if(scStr.equals("quit")){
			quit="quit";
			System.out.println("退出成功");
			break;
		}else if(Pattern.matches("^[a-zA-Z]+$", scStr)&& !scStr.equals("quit" )){
			System.out.println("请输入数字!!");
			continue;
		}
		else {
			
			fileNo=Integer.parseInt(scStr);
		}
		
		
		
		System.out.println("\n 正在转换中......");
		
		if (fileNo>fileNames.size() ) {
			System.out.println("非法输入");
			continue;
		}
		
		//所有文件
		else if(fileNo==fileNames.size()) {
			for (String name : fileNames) {
				pdfToDocment(inPut+name, isPage(), getOutPutType());
			}
			
		}else {
			
			pdfToDocment(inPut+fileNames.get(fileNo), isPage(),getOutPutType());
			
		}
		
		}while(!quit.equals("quit"));
		

    }
		
	
	
	public static int getPdfPage(String file) {
		File pdfFile = new File(file);
        PDDocument document = null;
        int pages = 0;
        try
        {
            document=PDDocument.load(pdfFile);
            pages = document.getNumberOfPages();
        }catch(Exception e)
        {
            //System.out.println(e);
        }
        try {
			document.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return pages;
	}

	
	

	public static void pdfToDocment(String pdfname,boolean isPage,String type) {

		File pdfFile = new File(pdfname);
        PDDocument document = null;
        Writer output=null;
        PDFTextStripper stripper=null;
        PDFText2HTML ph=null;
        int pages;
        String name;
        try
        
        {
        	;
        	document=PDDocument.load(pdfFile);
        	pages= document.getNumberOfPages();

        	stripper=new PDFTextStripper();
        	ph=new PDFText2HTML();
        	ph.setSortByPosition(true);
        	 stripper.setSortByPosition(true);
        	if (isPage) {
        		stripper.setStartPage(getStart());
                stripper.setEndPage(getEnd());
                
                ph.setStartPage(getStart());
                ph.setEndPage(getEnd());
			}else {
				stripper.setStartPage(1);
                stripper.setEndPage(pages);
			}
            
        	
        	if (type.equals("html")) {
				
        		name=pdfname.replace("inputpdf", "outputpdf").replace(".pdf", ".html");
        		FileUtil.SaveString2Text(ph.getText(document),name);
				checkFile(name);
        		
			}else if(type.equals("txt")) {
				name=pdfname.replace("inputpdf", "outputpdf").replace(".pdf", ".txt");
				FileUtil.SaveString2Text(stripper.getText(document), name);
				checkFile(name);
			}
			else {
				name=pdfname.replace("inputpdf", "outputpdf").replace(".pdf", ".doc");
				output = new OutputStreamWriter(new FileOutputStream(name),"UTF-8");
	            stripper.writeText(document, output);
	            checkFile(name);
			}
     
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        finally{
        	try {
				document.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        	if (output != null) {  
        		  
                // 关闭输出流  
                try {
					output.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
            }
        }
		
		
	}
	
	
	 public static String txt2String(File file){
	        StringBuilder result = new StringBuilder();
	        try{
	            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
	            String s = null;
	            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
	                result.append(s);
	            }
	            br.close();    
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return result.toString();
	    }
	
	 
	 public static String getOutPutType() {
			
		 return config.getString("type");
	}
	 
	 public static boolean isPage() {
		 
		if (config.getInt("ispage")==0) {
			return false;
		}else {
			return true;
		}
	}
	 
	 public static int getStart() {
		return config.getInt("start");
	}
	 
	 public static int getEnd() {
		 return config.getInt("end");
	 }
	 
	 
	 public static void checkFile(String fileName) {
		File file=new File(fileName);
		if (file.exists()) {
			System.out.println("转换成功,已经保存"+fileName);
		}
		else {
			System.out.println("转换失败");
		}
	}
	 
	 public static String getIntroduce() {
			
		 return config.getString("introduce");
	}
	
}
